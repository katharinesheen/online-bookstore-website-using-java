package constants;

public class IDatabase {

	public static final String DRIVER_NAME = "com.mysql.jdbc.Driver";
	public static final String CONNECTION_STRING = "jdbc:mysql://bookstore-mysql:3306/onlinebookstore";
	// public static final String USER_NAME = "root";
	// public static final String PASSWORD = "password";
	public static final String MYSQL_ROOT_PASSWORD = "password";
	public static final String MYSQL_USER= "mysql";
	public static final String MYSQL_PASSWORD= "mysql";

}
